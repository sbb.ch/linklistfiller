# Linklistfiller

Autofill-in links into offer list of F&F-Level 2 sites.

This script comes with absolutely no guarantee. Use at your own risk. Please enjoy time saved.

**How to**

1. Get a list of links from stakeholder.
2. The following link layouts are valid and will automatically be adjusted:
* Starting with https
* Starting with www only
* including parameter (?foobar)
* Direct CMS links

3. Open page to be edited
4. Open offer list
5. Execute script on site
6. Copy linklist and paste into prompt
7. See the script doing all the job for you.
8. Repeat in other languages if needed.


